package org.cursocoritel.buffer;
/**
 * 
 * @author Rafael Orozco Almario
 *
 */
public class BufferException extends RuntimeException {
	/**
	 * Constructor
	 */
  public BufferException() {
	  super() ;
  }
}
