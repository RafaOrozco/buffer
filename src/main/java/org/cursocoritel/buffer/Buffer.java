package org.cursocoritel.buffer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * Implements a circular buffer
 * 
 * @author Rafael Orozco Almario
 * @param <T>
 */
public class Buffer<T> {
	private Queue<T> circularBuffer;
	private int capacity;
	private int numberOfPuts = 0;
	private int numberOfGets = 0;
	private Logger log;

	/**
	 * Constructs an empty buffer.
	 * 
	 * @param bufferSize
	 */
	public Buffer(int bufferSize) {
		capacity = bufferSize;
		circularBuffer = new LinkedList<>();
		log = Logger.getLogger("log");
	}

	/**
	 * 
	 * Inserts the specified element into this buffer.
	 * 
	 * @param element
	 *            - the element to add
	 * @throws BufferException
	 *             if the buffer is full
	 */
	public void put(T element) throws BufferException {
		if (circularBuffer.size() == capacity)
			throw new BufferException();

		log.info("Element inserted");

		circularBuffer.add(element);
		numberOfPuts++;
	}

	/**
	 * @return the first element of the buffer
	 * @throws BufferException
	 *             if the buffer is empty
	 */
	public T get() throws BufferException {
		if (circularBuffer.isEmpty())
			throw new BufferException();

		T value = circularBuffer.remove();
		log.info("Element extracted");

		numberOfGets++;
		return value;
	}

	/**
	 * @return The number of elements in this collection
	 */
	public int getNumberOfElements() {
		return circularBuffer.size();
	}

	/**
	 * @return The number of holes in this collection
	 */
	public int getNumberOfHoles() {
		return capacity - circularBuffer.size();
	}

	/**
	 * @return The capacity in this collection
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * @return The possible number of operations
	 */
	public double getNumberOfOperations() {
		return (double) numberOfPuts + numberOfGets;
	}
}
