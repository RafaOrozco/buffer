package org.cursocoritel.buffertest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.LinkedList;
import java.util.Queue;

import org.cursocoritel.buffer.Buffer;
import org.cursocoritel.buffer.BufferException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import mockit.Deencapsulation;
/**
 * 
 * @author Rafael Orozco Almario
 *
 */
public class BufferTest {
	private int capacity = 10;
	private Buffer<Integer> buffer;

	/**
	 * Initialize buffer
	 */
	@Before
	public void setUp() {
		buffer = new Buffer<>(capacity);
	}

	/**
	 * Destroy buffer
	 */
	@After
	public void tearDown() {
		buffer = null;
	}

	/**
	 * Constructor creates a buffer with the right capacity
	 */
	@Test
	public void shouldConstructorCreateABufferWithTheRightCapacity() {
		assertEquals(capacity, (int) Deencapsulation.getField(buffer, "capacity"));
	}

	/**
	 * getCapacity() returns the right value
	 */
	@Test
	public void shouldGetCapacityReturnTheRightValue() {
		assertEquals(capacity, buffer.getCapacity());
	}

	/**
	 * get() returns a {@link BufferException} when the buffer is empty
	 */
	@Test(expected = BufferException.class)
	public void shouldGetThrowExceptionIfBufferIsEmpty() {
		buffer.get();
	}

	/**
	 * get() returns a value when buffer is not empty
	 */
	@Test
	public void shouldGetReturnAValueIfBufferIsNotEmpty() {
		Queue<Integer> list = new LinkedList<>();
		list.add(4);
		Deencapsulation.setField(buffer, "circularBuffer", list);
		assertNotNull(buffer.get());
	}

	/**
	 * get() returns the right value after buffer has an element
	 */
	@Test
	public void shouldGetReturnTheRightValueAfertBuffertHashAnElement() {
		Queue<Integer> list = new LinkedList<>();
		list.add(4);
		Deencapsulation.setField(buffer, "circularBuffer", list);
		assertEquals(4, (int) buffer.get());
	}

	/**
	 * put() returns a {@link BufferException} when the buffer is full
	 */
	@Test(expected = BufferException.class)
	public void shouldPutReturnAnExceptionIfBufferIsFull() {
		for (int i = 0; i <= capacity; i++) {
			buffer.put(i);
		}
	}

	/**
	 * getNumberOfElements() returns the right number of elements
	 */
	@Test
	public void shouldGetNumberOfElemetsReturnRightNumber() {
		int numberOfElements = capacity;
		for (int i = 0; i < numberOfElements; i++) {
			buffer.put(i);
		}
		assertEquals(numberOfElements, buffer.getNumberOfElements());
	}

	/**
	 * getNumberOfHoles() returns the right number of holes
	 */
	@Test
	public void shouldGetNumberOfHolesReturnRightNumber() {
		int elements = capacity - 2;
		for (int i = 0; i < elements; i++) {
			buffer.put(i);
		}
		assertEquals(capacity - elements, buffer.getNumberOfHoles());
	}

	/**
	 * getNumberOfOperations() returns the right number of operations
	 */
	@Test
	public void shouldGetNumberOfOperationsReturnRightNumber() {
		buffer.put(1);
		buffer.put(2);
		buffer.get();
		assertEquals(3, buffer.getNumberOfOperations(), -1e12);
	}

}
